module edit_distance;

enum EditType {
        None,
        Substitute,
        Insert,
        Delete
};

struct EDResult
{
        int        distance;
        string     text;
        EditType[] edits;
};

EDResult editDistance(string from, string to)
{
        EditDistance ed;
        return ed.calcDistance(from, to);
}

private struct EditDistance
{
    public:

        EDResult calcDistance(string from, string to)
        {
                _rows = cast(int)from.length;
                _cols = cast(int)to.length;
                _from = from;
                _to = to;

                auto cellCount = (_rows+1) * (_cols+1);
                if(_cells.length < cellCount) {
                        _cells = new Cell[cellCount];
                }

                // If the 'to' string is empty, the distance is
                // exactly {from.length} delete operations.
                foreach(int i; 0.._rows+1) {
                        auto cell = get(i, 0);
                        cell.dist = i;
                        if(i > 0) {
                                cell.type = EditType.Delete;
                                cell.editValue = from[i-1];
                                cell.parentRow = i - 1;
                                cell.parentCol = 0;
                        }
                }

                // If the 'from' string is empty, the distance is
                // exactly {to.length} insert operations.
                foreach(int j; 0.._cols+1) {
                        auto cell = get(0, j);
                        cell.dist = j;
                        if(j > 0) {
                                cell.type = EditType.Insert;
                                cell.editValue = to[j-1];
                                cell.parentRow = 0;
                                cell.parentCol = j - 1;
                        }
                }

                foreach(int j; 0.._cols) {
                        foreach(int i; 0.._rows) {
                                processCell(i, j);
                        }
                }

                return getResult();
        }

    private:

        struct Cell
        {
                int dist = 0;
                int parentRow = -1;
                int parentCol = -1;
                EditType type = EditType.None;
                char editValue = 0;
        }

        Cell* get(int row, int col)
        {
                return &(_cells[row * (_cols + 1) + col]);
        }

        void processCell(int i, int j)
        {
                auto row = i + 1;
                auto col = j + 1;
                auto cell = get(row, col);

                if(_from[i] == _to[j]) {
                        cell.dist = get(i, j).dist;
                        cell.parentRow = i;
                        cell.parentCol = j;
                        cell.type = EditType.None;
                        cell.editValue = 0;
                } else {
                        auto sub = get(i  , j  ).dist + 1;
                        auto ins = get(i+1, j  ).dist + 1;
                        auto del = get(i  , j+1).dist + 1;

                        import std.algorithm : min;
                        auto v = min(sub, ins, del);

                        cell.dist = v;

                        // Prefer substitutions over insertions, and
                        // insertions over deletions.
                        if(v == sub) {
                                cell.parentRow = i;
                                cell.parentCol = j;
                                cell.type = EditType.Substitute;
                                cell.editValue = _to[j];
                        } else if(v == ins) {
                                cell.parentRow = i+1;
                                cell.parentCol = j;
                                cell.type = EditType.Insert;
                                cell.editValue = _to[j];
                        } else if(v == del) {
                                cell.parentRow = i;
                                cell.parentCol = j+1;
                                cell.type = EditType.Delete;
                                cell.editValue = _from[i];
                        }
                }
        }

        EDResult getResult()
        {
                import std.array;

                EDResult result;
                result.distance = get(_rows, _cols).dist;
                auto text = _from.dup();
                result.edits = new EditType[_from.length];

                foreach(ref e; result.edits) {
                        e = EditType.None;
                }

                auto cell = get(_rows, _cols);

                while(true) {
                        auto pr = cell.parentRow;
                        auto pc = cell.parentCol;

                        if(cell.type != EditType.None) {

                                int i = 0;
                                int k = 0;
                                while(k < pr) {
                                        if(result.edits[i] != EditType.Delete) {
                                                k++;
                                        }
                                        i++;
                                }

                                if(cell.type == EditType.Substitute) {
                                        result.edits[i] = cell.type;
                                } else if(cell.type == EditType.Insert) {
                                        result.edits.insertInPlace(i, cell.type);
                                        text.insertInPlace(i, cell.editValue);
                                } else if(cell.type == EditType.Delete) {
                                        result.edits[i] = cell.type;
                                        text[i] = cell.editValue;
                                }
                        }

                        if(pr == -1) {
                                assert(pc == -1);
                                break;
                        }

                        cell = get(pr, pc);
                }

                result.text = text.idup;

                return result;
        }

    private:

        int    _rows = 0;
        int    _cols = 0;
        string _from;
        string _to;

        Cell[] _cells;
};
