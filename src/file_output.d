module file_output;

import std.stdio     : File;
import std.exception : enforce;
import basic_output  : BasicOutput;
import output        : Output;
import std.conv      : to;

class FileOutput : BasicOutput
{
    public:
        enum SampRate = 44100;

    public:
        this(string filename)
        {
                super(SampRate);
                _filename = filename;
        }

        void init()
        {
                _file = File(_filename, "wb");
        }

        void shutdown()
        {
                _file.close();
        }

        void startPlayback()
        {
        }

        void stopPlayback()
        {
        }

        void waitUntilFinished()
        {
                while(queueLength() > 0) {
                        auto tone = getSound(queueFront());
                        foreach(samp; tone) {
                                // 2-channel 16-bit signed PCM.
                                auto shortVal = to!short(samp * (short.max - 1));
                                _file.rawWrite([shortVal, shortVal]);
                        }
                        popQueue();
                }
        }

    private:
        File   _file;
        string _filename;
};

