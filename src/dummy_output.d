module dummy_output;

version (dummy_output) {

import output : Output;

class DummyOutput : Output
{
        override void init() {}
        override void shutdown() {}
        override int addSound(immutable(float)[] data) { return 1; }
        override void queueSound(int id) {}
        override int getSampleRate() { return 44100; }
        override void startPlayback() {}
        override void stopPlayback() {}
        override void waitUntilFinished() {}
};

} // version (dummy_output) {
